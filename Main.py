import logging

from aiogram import Bot, Dispatcher, executor, types
import os
#from aiogram.dispatcher import FSMContext
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, InputFile
from baza import saveTo

API_TOKEN = '5843517599:AAFJ8IaECEuRjwzelJs68ApsJlzx7Xn6y6k'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

async def on_startup(_):
    print('Bot вышел в online ...')

@dp.message_handler(commands=['help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    await message.reply("Привет!\nЯ EchoBot!\nЗапущен через aiogram.")

@dp.message_handler(commands=['tel'])
async def phone(message):
        keyboard1 = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        button_phone = types.KeyboardButton(text="Отправить телефон",
                                            request_contact=True)
        keyboard1.add(button_phone)
        await message.answer('Номер телефона', reply_markup=keyboard1)

@dp.message_handler(content_types=['contact'])
async def contact(message):
    if message.contact is not None:
        keyboard2 = types.ReplyKeyboardRemove()
        await message.answer('Вы успешно отправили свой номер', reply_markup=keyboard2)
        global phoneNumber,userId
        phoneNumber = int(str(message.contact.phone_number))
        userId     = int(str(message.contact.user_id))
#        print(phoneNumber,userId)
        saveTo(phoneNumber,userId)


@dp.message_handler()
async def echo(message: types.Message):
    # old style:
    # await bot.send_message(message.chat.id, message.text)
    await message.answer(message.text)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True,on_startup=on_startup)
